# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->
## 0.3.8 - 2025-01-30

- Fix python 3.12 compatibility issue

## 0.3.7 - 2023-09-21

- fix documentation display
- fix missing dialog (show_alert)
- formatting code

## 0.3.6 - 2023-09-20

- formatting to fix some flake8 issues

## 0.3.5 - 2023-02-27

- compute details points from multiple stations
- compute bearing PQ (V0) from sights on references
- new point has unknown Z by default
- add apparent level adjustment
- adaptative menu

## 0.3.4 - 2023-01-13

- fix Trimble processing
- add some critical messages and remove useless warnings

## 0.3.3 - 2022-11-30

- add and fix messages
- fix user input processing when editing observation

## 0.3.2 - 2022-11-24

- survey project is named like directory
- add import points
- fix messages and translation

## 0.3.1 - 2022-11-16

- embed pygeoif package
- fix some type errors

## 0.3.0 - 2022-10-28

- create new survey

## 0.1.0 - 2022-09-07

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
