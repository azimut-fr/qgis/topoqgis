<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

# TOPAZE

Topaze est une extension permettant de traiter les levers topographiques avec QGIS. Un lever
est constitué d’un dossier contenenant un projet QGIS du même nom ainsi que deux sous-dossiers
`db` (contenant le fichier geopackage) et `terrain` contenant des exemples de données
terrain.

Il utilise les carnets électroniques de terrain (théodolites et niveaux) les plus courants (Leica
et Trimble à ce jour). Les données issues de ces appareils de terrain sont converties dans un
format commun et regroupées dans la table `observations` qui contient les mesures et la
codification.
Un éditeur d’observations permet d’ajouter, modifier ou supprimer une ou plusieurs
observations.

Les fonctions de calculs de canevas (à développer) permettront de traiter tous les types de
polygonation par "compensation en bloc".

Il fournit aujourd’hui les fonctionnalités suivantes :

![Menu Topaze](../_static/images/menu1.png)

- Créer un nouveau lever : initialise un dossier avec les fichiers nécessaires (projet QGIS et bd
Geopackage notamment)

- Mettre à jour le carnet : convertit les données terrain du fichier leica ou Trimble en
format interne et les ajoute à la table `observations`

- Editer les observations : ouvre une boite de dialogue permettant de modifier le carnet
contenu dans la table `observations`

- Importer des points : l’importation se fait, pour l’instant, uniquement à partir d’un fichier au
format Topojis; les points importés sont enregistrés dans la table `points`.

- Calculer les points de détail : calcule et crée les points rayonnés dans la table `points`

- Effacer le carnet : réinitialise la table `observations`

Le traitement numérique d’un levé se décompose donc en cinq étapes:

- Nouveau lever,
- transfert des données de l’ enregistreur de terrain sur l’ ordinateur,
- mise à jour du carnet Topaze ("observations "),
- importation des points connus,
- TODO :calcul des points de canevas (XY, XYZ, XY puis Z) avec possibilité
    de calculer les altitudes par compensation d'observations d'un nivellement direct,
- calculs des points levés depuis chaque station

## Nouveau lever

Pour créer un nouveau lever, cliquer sur le bouton de navigation (...), créer un dossier et le sélectionner puis valider.

![Nouveau lever](../_static/images/nouveau_lever.png)

Topaze copie alors les fichiers nécessaires dans ce dossier et renomme le projet (.qgs)
du nom du dossier

/!\ il efface son contenu éventuel

## Mise à jour du carnet

La mise à jour du carnet Topaze ("observations ") consiste à recopier les observations
de levé et/ou la codification des points issus de votre carnet électronique de terrain
dans la table `observations` en mettant ces données dans un format utilisable
directement par Topaze.

![Mise à jour du carnet](../_static/images/maj_carnet.png)

- Pour mettre à jour ce fichier il suffit de sélectionner un fichier terrain dans la liste affichée (.gsi, .are). Le sous-répertoire terrain du projet est prévu pour les fichiers terrain.

- Cliquer sur Enregistrer pour mettre à jour la table «observations» avec le fichier terrain sélectionné.

## Editer le carnet

Avec l’éditeur d’observations, il est possible de sélectionner une observation pour la
modifier, la supprimer, ou bien ajouter une nouvelle observation après celle-ci.

Chaque fois qu’ une observation est sélectionnée, la ligne est analysée et les champs
appropriés sont activés. De ce fait il est très facile de modifier le type d’une ligne, en
indiquant, par exemple que le point 5002 est un point de détail et non une référence, ce
sans ressaisir les valeurs mesurées.

Les types de ligne et leur contenu peuvent être:

- **st** : No de station, Hauteur d'instrument (`hi`), V0 (`v0`)
   * Si la valeur du V0 est nulle, Topaze considère qu'il n'y a pas de V0
   * Si des références connues sont visées depuis la station, Topaze les intégrera même si
un V0 a été saisi. Le calcul en sera d'autant plus précis.
ref** : `No référence` `Angle horizontal` `Angle vertical` `Distance inclinée`
   * Un angle horizontal négatif ne sera pas pris en compte dans les calculs
   * Un angle vertical inférieur à 10 Grades ne sera pas pris en compte dans les calculs
   * Une distance inférieure à 1m ne sera pas prise en compte dans les calculs
- **pnt** : `No de point` `Angle horizontal` `Angle vertical` `Distance inclinée`
Un angle horizontal négatif ne sera pas pris en compte dans les calculs
Un angle vertical inférieur à 10 Gr ne sera pas pris en compte dans les calculs Une
distance inférieure à 1m ne sera pas prise en compte dans les calculs
- **hp** : Hauteur de prisme
Pour les points de détails, une hauteur de prisme égale à 99 m a pour effet de ne pas
calculer leur altitude
- **code** : `Code point` `Code ligne` `Type géométrie` `No ligne famille`
Les excentrements Avant, Droite et haut sont saisis en valeur positive
Les excentrements Arrière, Gauche et Bas sont saisis en valeur négative
- **xyz** : `Point`/`Station`/`Référence` `X` `Y` `Z`
Cette ligne doit être placée immédiatement après le point, la station ou la référence
concernée.
- **infos** : Texte de remarque

![Editer du carnet](../_static/images/editer_carnet.png)

Pour sélectionner une ligne il suffit de cliquer sur cette ligne.

Pour ajouter une ligne, saisir les valeurs, cliquez une fois sur la ligne après laquelle
vous désirez ajouter l’observation, puis appuyez sur le bouton Ajouter.

Pour modifier une ligne, saisir les valeurs, sélectionnez la ligne que vous désirez
modifier, puis appuyez sur le bouton Modifier.

Lorsque vous poussez le bouton Effacer, la ligne sélectionnée est effacée en mémoire.

**NB** : _Tant qu’aucune modification n’a été faite, le bouton Enregistrer est désactivé._

## Importer des points

![Importer des points](../_static/images/importer_points.png)

Choisir le fichier puis cliquer sur ouvrir. Ce fichier doit être au format Topojis :

```
S.50570:X=1352312.6096,Y=7222488.3463,Z=26.105,PH=7,PV=7,CS=1,MD=P_STATION,CA=TOPSTA
S.50571:X=1352270.3770,Y=7222495.1687,PH=7,CS=1,MD=P_STATION,CA=TOPSTA
S.25687:X=1352339.5100,Y=7222586.0400,Z=26.16400,PH=7,PV=7,MD=P_STATION,CA=TOPSTA
S.16836:X=1352368.5072,Y=7222455.6587,Z=26.20650,PH=7,PV=7,CS=1,MD=P_STATION,CA=TOPSTA
```

## Calculer les points de détail

A l’appel de cette commande, Topaze charge la table «observations » et calcule puis
crée les points dans la table `points`.
