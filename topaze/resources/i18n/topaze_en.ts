<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>EditObsDialog</name>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="26"/>
        <source>Observations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="49"/>
        <source>Edition</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="62"/>
        <source>Unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="67"/>
        <source>st</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="72"/>
        <source>ref</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="77"/>
        <source>pnt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="82"/>
        <source>hp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="87"/>
        <source>cpt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="92"/>
        <source>cli</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="97"/>
        <source>xyz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="102"/>
        <source>ch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="107"/>
        <source>da</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="112"/>
        <source>cmd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="117"/>
        <source>infos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="122"/>
        <source>code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="145"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="150"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="155"/>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="189"/>
        <source>Sloped dis.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="212"/>
        <source>Ver. Ang.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="235"/>
        <source>Hor. Ang.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="248"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="281"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="304"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="327"/>
        <source>Target Ht.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="350"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="442"/>
        <source>Comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="386"/>
        <source>Instr. Ht.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="419"/>
        <source>v0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="483"/>
        <source>Add</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="496"/>
        <source>Change</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="509"/>
        <source>Delete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="522"/>
        <source>Save</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="535"/>
        <source>Print selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="548"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="75"/>
        <source>Miscellaneous</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="124"/>
        <source>Report an issue</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="146"/>
        <source>Version used to save settings:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="168"/>
        <source>Help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="190"/>
        <source>Reset setttings to factory defaults</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="209"/>
        <source>Enable debug mode.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="218"/>
        <source>Debug mode (degraded performances)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PlgTranslator</name>
    <message>
        <location filename="../../toolbelt/translator.py" line="68"/>
        <source>Your selected locale ({}) is not available. Please consider to contribute with your own translation :). Contact the plugin maintener(s): {}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TopazePlugin</name>
    <message>
        <location filename="../../plugin_main.py" line="125"/>
        <source>&amp;Topaze</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="133"/>
        <source>Process field codification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="905"/>
        <source>Edit observations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="155"/>
        <source>Compute reference points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="166"/>
        <source>Update observations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="199"/>
        <source>Compute points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="210"/>
        <source>Purge observations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="221"/>
        <source>Help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="230"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="101"/>
        <source>PTOPO table missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="111"/>
        <source>Observations table missing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="412"/>
        <source>Unable to add points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="668"/>
        <source>Unable to get field data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="603"/>
        <source>Unable to update observations with field data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="609"/>
        <source>Observations updated</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="696"/>
        <source>Unable to set field observations in table</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="779"/>
        <source>Unable to erase observations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="835"/>
        <source>Unable to add observation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="883"/>
        <source>Unable to save field observations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="888"/>
        <source>Field observations data saved</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="79"/>
        <source>No translation file for {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="177"/>
        <source>New survey</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="538"/>
        <source>Unable to create new survey </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="544"/>
        <source>New survey created </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="188"/>
        <source>Import points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="788"/>
        <source>Observations erased</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="505"/>
        <source>{} surveyed point(s) computed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="649"/>
        <source> ({} created, {} updated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="544"/>
        <source> in {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="631"/>
        <source>Unable to open {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="609"/>
        <source> with field data from {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="644"/>
        <source>Unable to import points from {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="649"/>
        <source>{} point(s) imported from {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="905"/>
        <source>Do you want to save or cancel and drop your changes ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Topaze_import_points</name>
    <message>
        <location filename="../../import_points_dialog.ui" line="14"/>
        <source>TOPAZE: import points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../import_points_dialog.ui" line="42"/>
        <source>Points</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../import_points_dialog.ui" line="107"/>
        <source>Points file :</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Topaze_new_survey</name>
    <message>
        <location filename="../../new_survey_dialog.ui" line="14"/>
        <source>New survey</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../new_survey_dialog.ui" line="42"/>
        <source>Choose directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../new_survey_dialog.ui" line="107"/>
        <source>New survey folder :</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Topaze_update_observations</name>
    <message>
        <location filename="../../field_data_dialog.ui" line="42"/>
        <source>Field data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="107"/>
        <source>Field data file :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="14"/>
        <source>TOPAZE: update observations</source>
        <translation></translation>
    </message>
</context>
</TS>
