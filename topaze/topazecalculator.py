from math import pi, cos, sin, atan2

from .topazeutils import TopazeUtils

class TopazeCalculator:

    @classmethod
    def obs_to_xyz(cls, station, obs):
        """Return x, y, z computed with obs from station
        
        Keyword argumnts:
        station -- Station object
        obs -- measures for point from station
        
        Returns:
        x - east coordinate
        y - north coordinate
        z - elevation (only if Station.z is defined
        """
        x = y = z = None
        if obs.x is None:
            if obs.di and obs.av:
                # circle on left management
                if obs.av > 200.:
                    vert_angle = 400. - obs.av
                    hori_angle = 400. - obs.ah
                else:
                    vert_angle = obs.av
                    hori_angle = obs.ah
                dist = obs.di * sin(vert_angle / 200. * pi) 
                dz = obs.di * cos(vert_angle / 200. * pi) 
            elif obs.dh is not None:
                dist = obs.dh
            if obs.dz is not None:
                dz = obs.dz
            dx = dist * sin((hori_angle + station.v0) / 200. * pi)
            dy = dist * cos((hori_angle + station.v0) / 200. * pi)
            x = station.x + dx
            y = station.y + dy
            if station.hi is not None and obs.hp is not None:
                a_l_a = TopazeUtils.apparent_level_adjustment(dist)
                z = station.z + dz + station.hi - obs.hp + a_l_a
        elif obs.x is not None and obs.y is not None:
            x = obs.x
            y = obs.y
            if obs.z is not None:
                z = obs.z

        return x, y, z
    
    @staticmethod
    def gon_0_400(alpha):
        while alpha < 0:
            alpha = alpha + 400
        while alpha > 400:
            alpha = alpha - 400
        return alpha

    @staticmethod
    def bearing_gon(x1, y1, x2, y2):
        bea = atan2(x2-x1,y2-y1)
        if bea < 0:
            bea = bea + pi * 2
        bea = bea * 200. / pi
        return bea

    @staticmethod
    def compute_bearing_pq(station, references):
        bpq_array = []
        v0 = -10
        idx = 0
        for ref in references:
            bpq = 0
            try:
                if ref.av > 200:
                    ah = TopazeCalculator.gon_0_400(ref.ah - 200) 
                else:
                    ah = ref.ah
                bpq = TopazeCalculator.bearing_gon(station.x, station.y, ref.x, ref.y) - ah
                bpq = TopazeCalculator.gon_0_400(bpq) 
            except Exception as e:
                pass

            if v0 >= 0.:
                if bpq > v0 and (bpq - v0) > 400:
                    v0 += 400
                elif bpq < v0 and (v0 - bpq) > 400:
                    bpq += 400

            v0 = (v0 * idx + bpq) / (idx + 1)
            idx += 1
            bpq_array.append(bpq)

        bearing_pq = 0
        for bpq in bpq_array:
            bearing_pq += bpq
        bearing_pq /= len(bpq_array)
        return bearing_pq
