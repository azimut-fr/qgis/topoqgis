class Station:

    __slots__ = ('matricule', 'x', 'y', 'z','hi', 'v0')
    
    def __init__(self, matricule:str):
        """ Constructor
        :param matricule: station name
        """
        self.matricule = matricule
        self.x = None
        self.y = None
        self.z = None
        self.hi = None
        self.v0 = None