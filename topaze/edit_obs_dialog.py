# -*- coding: utf-8 -*-
"""
/***************************************************************************
 EditObsDialog
                                 A QGIS plugin
 Topaze
                             -------------------
        begin                : 2022-09-30
        git sha              : $Format:%H$
        copyright            : (C) 2022 by Jean-Marie ARSAC
        email                : jmarsac@arsac.wf
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt5 import QtCore, QtWidgets, uic

from qgis.core import QgsProject

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "edit_obs_dialog.ui")
)


class EditObsDialog(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(EditObsDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

    def load_observations(self, field_rows):
        """ load observations in dialog list """
        self.listWidget_observations.clear()
        if field_rows:
            self.listWidget_observations.addItems(field_rows)

    def update_combobox_types(self, type_obs, type_pt):
        self.comboBox_type_obs.setVisible(True)
        self.comboBox_type_obs.setEnabled(True)
        self.comboBox_type_obs.setCurrentText(type_obs)
        self.comboBox_type_obs.update()
        if type_obs == 'xyz':
            if type_pt is not None:
                self.comboBox_type_point.setCurrentText(type_pt)
        else:
            self.comboBox_type_point.setCurrentText(type_pt)
        self.comboBox_type_point.update()

    def load_observation_fields(self, type_obs, values):
        if type_obs in ['st', 'ref', 'pnt', 'xyz']:
            if 'pnt' in values:
                self.lineEdit_matricule.setText(values['pnt'])
            elif 'st' in values:
                self.lineEdit_matricule.setText(values['st'])
            elif 'ref' in values:
                self.lineEdit_matricule.setText(values['ref'])
            elif 'xyz' in values:
                self.lineEdit_matricule.setText(values['xyz'])
            if type_obs == 'st':
                if 'hi' in values and values['hi']:
                    self.lineEdit_instrument_height.setText(values['hi'])
                if 'v0' in values and values['v0']:
                    self.lineEdit_v0.setText(values['v0'])
            elif type_obs == 'xyz':
                if 'x' in values and values['x']:
                    self.lineEdit_x.setText(values['x'])
                if 'y' in values and values['y']:
                    self.lineEdit_y.setText(values['y'])
                if 'z' in values and values['z']:
                    self.lineEdit_z.setText(values['z'])
            else: # 'ref' or 'pnt'
                if 'ah' in values and values['ah']:
                    self.lineEdit_horizontal_angle.setText(values['ah'])
                if 'av' in values and values['av']:
                    self.lineEdit_vertical_angle.setText(values['av'])
                if 'di' in values and values['di']:
                    self.lineEdit_sloped_distance.setText(values['di'])
        elif type_obs == 'hp':
            if type_obs in values and values[type_obs]:
                self.lineEdit_target_height.setText(values[type_obs])
        elif type_obs == 'code':
            if type_obs in values and values[type_obs]:
                self.lineEdit_code.setText(values['code'])
        elif type_obs == 'rem':
            if type_obs in values and values[type_obs]:
                self.lineEdit_comment.setText(values['rem'])
        elif type_obs == 'infos':
            if type_obs in values and values[type_obs]:
                self.lineEdit_infos.setText(values['infos'])

    def observation_fields_to_dict(self, type_obs, type_pt:str = None):
        dico = dict()
        if type_obs in ['st', 'ref', 'pnt', 'xyz']:
            if type_obs == 'st':
                dico[type_obs] = self.lineEdit_matricule.text()
                dico['hi'] = self.lineEdit_instrument_height.text()
                s_val = self.lineEdit_v0.text()
                if s_val:
                    dico['v0'] = self.lineEdit_v0.text()
            elif type_obs == 'xyz':
                if type_pt:
                    if type_pt == 'S':
                        type_obs = 'st'
                    elif type_pt  == 'R':
                        type_obs = 'ref'
                    else:
                        type_obs = 'pnt'
                else:
                    type_obs = 'pnt'
                dico[type_obs] = self.lineEdit_matricule.text()
                dico['x'] = self.lineEdit_x.text()
                dico['y'] = self.lineEdit_y.text()
                s_val = self.lineEdit_z.text()
                if s_val:
                    dico['z'] = self.lineEdit_z.text()
            else: # 'ref' or 'pnt'
                dico[type_obs] = self.lineEdit_matricule.text()
                s_val = self.lineEdit_horizontal_angle.text()
                if s_val:
                    dico['ah'] = self.lineEdit_horizontal_angle.text()
                s_val = self.lineEdit_vertical_angle.text()
                if s_val:
                    dico['av'] = self.lineEdit_vertical_angle.text()
                s_val = self.lineEdit_sloped_distance.text()
                if s_val:
                    dico['di'] = self.lineEdit_sloped_distance.text()
        elif type_obs == 'hp':
            dico[type_obs] = self.lineEdit_target_height.text()
        elif type_obs == 'code':
            dico[type_obs] = self.lineEdit_code.text()
        elif type_obs == 'rem':
            dico['rem'] = self.lineEdit_comment.text()
        elif type_obs == 'infos':
            dico['infos'] = self.lineEdit_infos.text()
        return dico

    def update_state_of_input_fields(self, type_obs, type_pt:str = None):
        # type_obs, type_pt = TopazeUtils.types_obs(row)
        # obs_dict = TopazeUtils.obs_str_to_dict(row)
        self.comboBox_type_point.setEnabled(False)
        self.comboBox_type_point.setVisible(False)
    
        self.lineEdit_matricule.setEnabled(False)
        self.lineEdit_instrument_height.setEnabled(False)
        self.lineEdit_v0.setEnabled(False)
        self.lineEdit_horizontal_angle.setEnabled(False)
        self.lineEdit_vertical_angle.setEnabled(False)
        self.lineEdit_sloped_distance.setEnabled(False)
        self.lineEdit_x.setEnabled(False)
        self.lineEdit_y.setEnabled(False)
        self.lineEdit_z.setEnabled(False)
        self.lineEdit_target_height.setEnabled(False)
        self.lineEdit_code.setEnabled(False)
        self.lineEdit_comment.setEnabled(False)
        self.lineEdit_infos.setEnabled(False)

        self.lineEdit_matricule.setVisible(False)
        self.lineEdit_instrument_height.setVisible(False)
        self.lineEdit_v0.setVisible(False)
        self.lineEdit_horizontal_angle.setVisible(False)
        self.lineEdit_vertical_angle.setVisible(False)
        self.lineEdit_sloped_distance.setVisible(False)
        self.lineEdit_x.setVisible(False)
        self.lineEdit_y.setVisible(False)
        self.lineEdit_z.setVisible(False)
        self.lineEdit_target_height.setVisible(False)
        self.lineEdit_code.setVisible(False)
        self.lineEdit_comment.setVisible(False)
        self.lineEdit_infos.setVisible(False)

        self.label_instrument_height.setVisible(False)
        self.label_v0.setVisible(False)
        self.label_horizontal_angle.setVisible(False)
        self.label_vertical_angle.setVisible(False)
        self.label_sloped_distance.setVisible(False)
        self.label_x.setVisible(False)
        self.label_y.setVisible(False)
        self.label_z.setVisible(False)
        self.label_target_height.setVisible(False)
        self.label_code.setVisible(False)
        self.label_comment.setVisible(False)
        self.label_infos.setVisible(False)
        if type_obs in ["st", "ref", "pnt", "xyz"]:
            self.lineEdit_matricule.setVisible(True)
            self.lineEdit_matricule.setEnabled(True)
            if type_obs == "st":
                self.label_instrument_height.setVisible(True)
                self.label_v0.setVisible(True)
                self.lineEdit_instrument_height.setVisible(True)
                self.lineEdit_v0.setVisible(True)
                self.lineEdit_instrument_height.setEnabled(True)
                self.lineEdit_v0.setEnabled(True)
            elif type_obs == "xyz":
                self.comboBox_type_point.setEnabled(True)
                self.comboBox_type_point.setVisible(True)
                if type_pt is not None:
                    self.comboBox_type_point.setCurrentText(type_pt)
                self.label_x.setVisible(True)
                self.label_y.setVisible(True)
                self.label_z.setVisible(True)
                self.lineEdit_x.setVisible(True)
                self.lineEdit_y.setVisible(True)
                self.lineEdit_z.setVisible(True)
                self.lineEdit_x.setEnabled(True)
                self.lineEdit_y.setEnabled(True)
                self.lineEdit_z.setEnabled(True)
            else: # "ref" or "pnt"
                self.label_horizontal_angle.setVisible(True)
                self.label_vertical_angle.setVisible(True)
                self.label_sloped_distance.setVisible(True)
                self.lineEdit_horizontal_angle.setVisible(True)
                self.lineEdit_vertical_angle.setVisible(True)
                self.lineEdit_sloped_distance.setVisible(True)
                self.lineEdit_horizontal_angle.setEnabled(True)
                self.lineEdit_vertical_angle.setEnabled(True)
                self.lineEdit_sloped_distance.setEnabled(True)
        elif type_obs == "hp":
            self.label_target_height.setEnabled(True)
            self.lineEdit_target_height.setEnabled(True)
            self.lineEdit_target_height.setVisible(True)
        elif type_obs == "code":
            self.label_code.setEnabled(True)
            self.lineEdit_code.setEnabled(True)
            self.lineEdit_code.setVisible(True)
        elif type_obs == "rem":
            self.label_comment.setEnabled(True)
            self.lineEdit_comment.setEnabled(True)
            self.lineEdit_comment.setVisible(True)
        elif type_obs == "infos":
            self.label_infos.setEnabled(True)
            self.lineEdit_infos.setEnabled(True)
            self.lineEdit_infos.setVisible(True)
