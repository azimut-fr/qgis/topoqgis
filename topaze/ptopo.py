class PtopoConst:
    NO_Z = -999.

class Ptopo:

    __slots__ = ('matricule', 'type', 'x', 'y', 'z', 'codes', 'prec_xy', 'prec_z')

    def __init__(self, matricule:str , type:str = 'P', x:float = None, y:float = None,
        z:float = PtopoConst.NO_Z, codes:str = None, prec_xy = 'C', prec_z = 'C'):
        """ Constructor
        :param matricule: ptopo name
        """
        self.matricule = matricule
        self.type = type
        self.x = x
        self.y = y
        self.z = z
        self.codes = codes
        self.prec_xy = prec_xy
        self.prec_z = prec_z
