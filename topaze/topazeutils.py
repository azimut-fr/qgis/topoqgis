import math

from ctypes.wintypes import PINT

from qgis.core import QgsExpression, QgsFeatureRequest, QgsPoint
from qgis.PyQt.QtCore import QRegularExpression, QRegularExpressionMatch

from .ptopo import Ptopo
from .station import Station

class TopazeUtils:

    @staticmethod
    def find_file_encoding(fullfilepath):
        import codecs
        encodings = ['windows-1250', 'iso-8859-1', 'iso-8859-15',
            'windows-1252', 'utf-8']
        for e in encodings:
            try:
                fh = codecs.open(fullfilepath, 'r', encoding=e)
                fh.read()
                fh.seek(0)
            except UnicodeDecodeError:
                fh.close()
                return None
            else:
                fh.close()
                return e
        return None

    @staticmethod
    def get_field_data_type(fullfilepath):
        try:
            with open( fullfilepath, 'r',
                encoding=TopazeUtils.find_file_encoding(fullfilepath)
                ) as field_file:
                buffer = field_file.read()
                field_file.close()
                regex = QRegularExpression(
                    "(^\*11[0-9]{4}[-+][-0-9_A-Za-z.]{16} )|"
                    "(^\*41[0-9]{4}+[-0-9_A-Za-z.?]{16} )")
                if regex.match(buffer[0:32]).hasMatch():
                    return "LEICA"
                regex = QRegularExpression(
                    "(^11[0-9]{4}[-+][-0-9a-zA-Z_.]{8} )|"
                    "(^41[0-9]{4}[-+][-0-9a-zA-Z_.?]{8} )")
                if regex.match(buffer[0:32]).hasMatch():
                    return "LEICA"
                regex = QRegularExpression(
                    "(^50=)|(^4=)|(^5=)|(^7=)|(^8=)|(^9=)|"
                    "(\n50=)|(\n4=)|(\n5=)|(\n7=)|(\n8=)|(\n9=)|"
                    "( 4=)|( 5=)|( 7=)|( 8=)|( 9=)")
                if regex.match(buffer[0:32]).hasMatch():
                    return "TRIMBLE"
                return None
        except Exception as e:
            print(str(e))
            return None

    @staticmethod
    def type_obs(obs):
        """ Return type of observation in string or dict.
        
        Keyword arguments:
            obs -- string containing observation (ex: 'st=S1 hi=1.65' or {'st': 'S1', 'hi': 1.65})
            
        Returns:
            type as first string before '=' ('st', 'ref', 'pnt', 'hp', 'hpp', 'code', 'xyz') 
        """
        type_obs = None
        if isinstance(obs, str):
            idx = obs.index("=")
            if idx > 0:
                type_obs = obs[:idx]
                if "x=" in obs and "y=" in obs:
                    type_obs = "xyz"
        elif isinstance(obs, dict):
            type_obs = obs.keys[0]

        return type_obs
    
    @staticmethod
    def types_obs(obs):
        """ Return types of observation and values in string or dict.
        
        Keyword arguments:
            obs -- string containing observation (ex: 'st=S1 hi=1.65' or {'st': 'S1', 'hi': 1.65})
            
        Returns:
            type_obs as first string before '=' ('st', 'ref', 'pnt', 'hp', 'hpp',
            'code'
            type_pt if values are x, y and z
        """
        type_obs = None
        type_pt = None
        if isinstance(obs, str) and '=' in obs:
            idx = obs.index("=")
            if idx > 0:
                type_obs = obs[:idx]
                if "x=" in obs and "y=" in obs:
                    if type_obs == "st":
                        type_pt = "S"
                    elif type_obs == "ref":
                        type_pt = "R"
                    elif type_obs == "pnt":
                        type_pt = "P"
                    type_obs = "xyz"

        elif isinstance(obs, dict) and len(obs.keys()):
            type_obs = obs.keys[0]
            if "x" in obs.keys() and "y" in obs.keys():
                if type_obs == "st":
                    type_pt = "S"
                elif type_obs == "ref":
                    type_pt = "R"
                elif type_obs == "pnt":
                    type_pt = "P"
                type_obs = "xyz"
    
        return type_obs, type_pt
            
    @staticmethod
    def dict_to_obs_str(dico):
        rows = []
        if 'st' in dico.keys():
            if 'x' in dico.keys() and 'y' in dico.keys():
                if 'z' in dico.keys():
                    rows.append(f"st={dico['st']} x={dico['x']} y={dico['y']} z={dico['z']}")
                else:
                    rows.append(f"st={dico['st']} x={dico['x']} y={dico['y']}")
            elif 'hi' in dico.keys() and dico['hi']:
                if 'v0' in dico.keys():
                    rows.append(f"st={dico['st']} hi={dico['hi']} v0={dico['v0']}")
                else:
                    rows.append(f"st={dico['st']} hi={dico['hi']}")
            elif 'hi' in dico.keys():
                rows.append(f"st={dico['st']} hi=0")
        elif 'pnt' in dico.keys() or 'ref' in dico.keys():
            if 'pnt' in dico.keys():
                key = 'pnt'
            elif 'ref' in dico.keys():
                key = 'ref'
            if 'x' in dico.keys() and 'y' in dico.keys():
                if 'z' in dico.keys():
                    rows.append(f"{key}={dico[key]} x={dico['x']} y={dico['y']} z={dico['z']}")
                else:
                    rows.append(f"{key}={dico[key]} x={dico['x']} y={dico['y']}")
            if 'ah' in dico.keys():
                if  'av' in dico.keys():
                    av = dico['av']
                    if 'di' in dico.keys():
                        rows.append(f"{key}={dico[key]} ah={dico['ah']} av={dico['av']} di={dico['di']}")
                    else:
                        rows.append(f"{key}={dico[key]} ah={dico['ah']} av={dico['av']}")
                else:
                    rows.append(f"{key}={dico[key]} ah={dico['ah']}")
        elif 'hp' in dico.keys():
            rows.append(f"hp={dico['hp']}")
        elif 'code' in dico.keys():
            rows.append(f"code={dico['code']}")
        elif 'rem' in dico.keys():
            rows.append(f"rem={dico['rem']}")
        elif 'infos' in dico.keys():
            rows.append(f"infos={dico['infos']}")
        else:
            print(dico)
        return rows

    @staticmethod
    def obs_str_to_dict(row):
        """Return dict from observation row
        
        Keyword arguments:
            row -- string containing observation (ex: 'st=S1 hi=1.65')
            
        Returns:
            dict - with values from row (ex: {'st': S1, 'hi': 1.65})
        """
        dico = dict()
        if row.startswith("code="):
            str_array = [f"code={row[5:]}"]
        elif row.startswith("infos="):
            str_array = [f"infos={row[6:]}"]
        else:
            str_array = row.split()
        for s in str_array:
            field = s.split("=")
            dico[field[0]] = field[1]
    
        return dico

    @staticmethod
    def apparent_level_adjustment(dh):
        earth_radius = 6370000.
        adjustment = 0.42 * pow(dh,2.) / earth_radius;
        print(f"correction niveau apparent {adjustment}")
        return adjustment


    @staticmethod
    def distance2d(p1, p2):
        return math.hypot(p2.x - p1.x, p2.y - p1.y)

    @staticmethod
    def squared_distance2d(p1, p2):
        return math.hypot(p2.x - p1.x, p2.y - p1.y)

    @staticmethod
    def find_references_in_obs_array(obs_array, 
        start_index: int=0, end_index: int=1024000):
        references = []
        idx = start_index - 1
        for obs in obs_array[start_index:end_index]:
            idx += 1
            if idx > end_index or obs.type == "st":
                return references, idx - 1
            if obs.type == "ref":
                references.append(obs)
        return references, idx - 1
        
    @staticmethod
    def find_station_in_obs_array(obs_array, start_index: int=0, 
        end_index: int=1024000):
        station = None
        idx = start_index - 1
        for obs in obs_array[start_index:end_index]:
            idx += 1
            if idx > end_index:
                return station, -1
            if obs.type == "st":
                if station is None:
                    station = Station(obs.matricule)
                if obs.hi is not None:
                    station.hi = obs.hi
                if obs.v0 is not None:
                    station.v0 = obs.v0 
                if obs.x is not None:
                    station.x = obs.x 
                if obs.y is not None:
                    station.y = obs.y 
                if obs.z is not None:
                    station.z = obs.z 
                return station, idx
        return station, -1

    @staticmethod
    def get_coordinates_from_references(references, ptopo_array, ptopo_layer):
        for ref in references:
            pt = TopazeUtils.get_ptopo_by_matricule(ref.matricule, ptopo_array, ptopo_layer)
            if pt:
                ref.x = pt.x
                ref.y = pt.y
                ref.z = pt.z

    @staticmethod
    def process_duplicated(old_pt, new_pt, tol_xy, tolerance_z, **options):
        d = TopazeUtils.squared_distance2d(old_pt, new_pt)
        z = dz = None
        if new_pt.z and old_pt.z:
            dz = abs(new_pt.z - old_pt.z)
        elif old_pt.z:
            z = old_pt.z
        elif new_pt.z:
            z = new_pt.z
        tolerance_xy = tol_xy * tol_xy
        if not tolerance_z:
            tolerance_z = tol_xy
        in_tolerance_xy = 'average'
        in_tolerance_z = 'average'
        out_tolerance_xy = 'use_new'
        out_tolerance_z = 'use_new'
        if 'in_tolerance_xy' in options:
            in_tolerance_z = in_tolerance_xy = options['in_tolerance_xy']
        if 'in_tolerance_z' in options:
            in_tolerance_z = options['in_tolerance_z']
        if 'out_tolerance_xy' in options:
            out_tolerance_z = out_tolerance_xy = options['out_tolerance_xy']
        if 'out_tolerance_z' in options:
            out_tolerance_z = options['out_tolerance_z']
        if d <= tolerance_xy:
            if dz:
                if dz < tolerance_z:
                    if in_tolerance_z == 'average':
                        z = (old_pt.z + new_pt.z) / 2.
                    elif in_tolerance_z == 'keep_old':
                        z = old_pt.z
                    elif in_tolerance_z == 'use_new':
                        z = new_pt.z
                else:
                    if out_tolerance_z == 'average':
                        z = (old_pt.z + new_pt.z) / 2.
                    elif out_tolerance_z == 'keep_old':
                        z = old_pt.z
                    elif out_tolerance_z == 'use_new':
                        z = new_pt.z
            if in_tolerance_xy == 'average':
                x = (old_pt.x + new_pt.x) / 2.
                y = (old_pt.y + new_pt.y) / 2.
            elif in_tolerance_xy == 'keep_old':
                x = old_pt.x
                y = old_pt.y
            elif in_tolerance_xy == 'use_new':
                x = new_pt.x
                y = new_pt.y
        else:
            if dz:
                if dz < tolerance_z:
                    if in_tolerance_z == 'average':
                        z = (old_pt.z + new_pt.z) / 2.
                    elif in_tolerance_z == 'keep_old':
                        z = old_pt.z
                    elif in_tolerance_z == 'use_new':
                        z = new_pt.z
                else:
                    if out_tolerance_z == 'average':
                        z = (old_pt.z + new_pt.z) / 2.
                    elif out_tolerance_z == 'keep_old':
                        z = old_pt.z
                    elif out_tolerance_z == 'use_new':
                        z = new_pt.z
            if out_tolerance_xy == 'average':
                x = (old_pt.x + new_pt.x) / 2.
                y = (old_pt.y + new_pt.y) / 2.
            elif out_tolerance_xy == 'keep_old':
                x = old_pt.x
                y = old_pt.y
            elif out_tolerance_xy == 'use_new':
                x = new_pt.x
                y = new_pt.y
        return x, y, z

    @staticmethod
    def ptopo_exists_in_array(matricule, ptopo_array):
        """
        Return True if Ptopo with matricule exists in array
        
        Keyword arguments:
        matricule -- ptopo matricule (ex: 'P1')
        ptopo_array -- array Ptopo objects
        
        Returns:
        True - if exists False in others cases
        """
        if ptopo_array:
            for ptopo in ptopo_array:
                if matricule == ptopo.matricule:
                    return True
        return False

    @staticmethod
    def ptopo_exists_in_layer(matricule, ptopo_layer):
        """
        Return True if Ptopo with matricule exists in ptopo_layer
        
        Keyword arguments:
        matricule -- ptopo matricule (ex: 'P1')
        ptopo_layer -- layer containing Ptopo objects
        
        Returns:
        True - if exists False in others cases
        """
        if ptopo_layer:
            selection = ptopo_layer.getFeatures(QgsFeatureRequest(
                QgsExpression(f"\"matricule\" = '{matricule}'")))
            for feature in selection:
                return True
        return False

    @staticmethod
    def get_ptopo_by_matricule(matricule, ptopo_array=None, ptopo_layer=None):
        """ Return object if Ptopo with matricule exists in layer
        
        Keyword arguments:
        matricule -- ptopo matricule (ex: 'P1')
        ptopo_array -- array of Ptopo objects (None by default)
        layer -- layer containing Ptopo objects (None by default)
        
        Returns:
        ptopo - Ptopo object if found else None

        If ptopo_array and ptopo_layer are provided, function looks for Ptopo
        in ptopo_layer firstly
        """
        if ptopo_layer:
            selection = ptopo_layer.getFeatures(QgsFeatureRequest(
                QgsExpression(f"\"matricule\" = '{matricule}'")))
            for feature in selection:
                ptopo = Ptopo(matricule)
                ptopo.type = feature.attribute('type_pt')
                geom = feature.geometry()
                pt = geom.constGet()
                ptopo.x = pt.x()
                ptopo.y = pt.y()
                ptopo.z = pt.z()
                ptopo.prec_xy = feature.attribute('prec_xy')
                ptopo.prec_z = feature.attribute('prec_z')
                ptopo.codes = feature.attribute('codes')
                return ptopo
        if ptopo_array:
            for ptopo in ptopo_array:
                if matricule == ptopo.matricule:
                    return ptopo
        return None

    @staticmethod
    def get_feature_by_matricule(matricule, ptopo_layer):
        """ Return feature if Ptopo with matricule exists in layer
        
        Keyword arguments:
        matricule -- ptopo matricule (ex: 'P1')
        layer -- layer containing Ptopo objects
        
        Returns:
        feature - Ptopo feature if found else None

        """
        if ptopo_layer:
            selection = ptopo_layer.getFeatures(QgsFeatureRequest(
                QgsExpression(f"\"matricule\" = '{matricule}'")))
            for feature in selection:
                return feature
        return None

    @staticmethod
    def find_ptopo_in_array(ptopo_array, matricule):
        """ Return array index if Ptopo with matricule exists in Ptopo array
        
        Keyword arguments:
        ptopo_array -- array of Ptopo objects
        matricule -- ptopo matricule (ex: 'P1')
        
        Returns:
        idx - Ptopo index in array if found else -1
        
        Keyword arguments:
        ptopo_array -- array of Ptopo objects
        matricule -- ptopo matricule (ex: 'P1')
        
        Returns:
        idx - Ptopo index in array if found else -1
        """
        idx = -1
        for ptopo in ptopo_array:
            idx = idx + 1
            if matricule == ptopo.matricule:
                return idx
        return idx

    @staticmethod
    def update_xyz_in_array(ptopo_array, matricule, x, y, z):
        """
        Update ptopo if matricule exists in Ptopo array
        
        Keyword arguments:
        ptopo_array -- array of Ptopo objects
        matricule -- ptopo matricule (ex: 'P1')
        x,y,z -- new coordinates
        
        Returns:
        True - if ptopo has been updated else False
        """
        status = False
        idx = -1
        for ptopo in ptopo_array:
            idx = idx + 1
            if matricule == ptopo.matricule:
                ptopo_array[idx].x = x
                ptopo_array[idx].y = y
                ptopo_array[idx].z = z
                return True
        return status