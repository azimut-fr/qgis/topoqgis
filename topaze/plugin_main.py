#! python3  # noqa: E265

"""
    Main plugin module.
"""
import importlib
import os
import shutil
from functools import partial
from math import pi
from pathlib import Path

from PyQt5.QtWidgets import QDialogButtonBox, QMessageBox, QProgressBar

# PyQGIS
from qgis.core import (
    Qgis,
    QgsApplication,
    QgsFeature,
    QgsGeometry,
    QgsProject,
    QgsVectorLayerUtils,
)
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication, QSettings, QUrl
from qgis.PyQt.QtGui import QDesktopServices, QIcon
from qgis.PyQt.QtWidgets import QAction, QToolBar
from qgis.utils import showPluginHelp

from topaze.__about__ import DIR_PLUGIN_ROOT, __title__, __uri_homepage__
from topaze.gui.dlg_settings import PlgOptionsFactory
from topaze.toolbelt import PlgLogger, PlgTranslator

# project
from topaze.totalopenstation import formats
from topaze.totalopenstation.formats import BUILTIN_INPUT_FORMATS
from topaze.totalopenstation.output.tops_azimut import OutputFormat

from .edit_obs_dialog import EditObsDialog
from .field_data_dialog import FieldDataDialog
from .import_points_dialog import ImportPointsDialog
from .new_survey_dialog import NewSurveyDialog
from .observation import Observation
from .ptopo import Ptopo, PtopoConst
from .ptopoutils import PtopoUtils
from .station import Station
from .topazecalculator import TopazeCalculator
from .topazeutils import TopazeUtils

# ############################################################################
# ########## Classes ###############
# ##################################


class TopazePlugin:
    OBSERVATIONS_LAYER_NAME = "observations"
    PTOPO_LAYER_NAME = "points"

    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        print("__init__")
        # initialize locale
        locale = QSettings().value("locale/userLocale")
        if locale is not None:
            locale = locale[0:2]
            locale_path = (
                Path(__file__).parent / "resources/i18n" / f"topaze_{locale}.qm"
            )
            if locale_path.exists():
                # translation
                plg_translation_mngr = PlgTranslator()
                translator = plg_translation_mngr.get_translator()
                if translator:
                    QCoreApplication.installTranslator(translator)
                self.tr = plg_translation_mngr.tr

            else:
                iface.messageBar().pushMessage(
                    "No translation file for {}".format(locale)
                )

        self.iface = iface
        self.toolbar = None
        self.actions = None
        self.menu = None
        self.dlg_field_data = FieldDataDialog()
        self.dlg_import_points = ImportPointsDialog()
        self.dlg_new_survey = NewSurveyDialog()
        self.dlg_edit_obs = EditObsDialog()
        self.dlg_edit_obs.pushButton_print.setVisible(False)
        self.dlg_edit_obs.pushButton_print.setEnabled(False)
        self.action_compute_point = None
        self.log = PlgLogger().log
        self._observations_layer = None
        self._ptopo_layer = None
        QgsProject.instance().cleared.connect(self.topaze_project_cleared)
        self.iface.projectRead.connect(self.topaze_project_read)
        print("self.iface.projectRead.connect(self.topaze_project_read)")
        if QgsProject.instance():
            self.topaze_project_read()

    def initGui(self):
        """Set up plugin UI elements."""
        print("initGui")
        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        self.actions = []
        self.menu = self.tr("&Topaze")
        self.toolbar = QToolBar("Topaze")
        self.toolbar.setObjectName("Topaze-toolbar")
        self.toolbar.setToolTip("Topaze")
        self._current_obs = None
        self._previous_obs = None

        # -- Actions
        self.action_new_survey_dialog = QAction(
            QIcon(":plugins/topaze/resources/images/default_icon.png"),
            self.tr("New survey", context="TopazePlugin"),
            self.iface.mainWindow(),
        )
        self.action_new_survey_dialog.triggered.connect(self.new_survey_dialog)
        self.action_new_survey_dialog.setEnabled(True)
        # self.toolbar.addAction(self.action_new_survey_dialog)

        self.action_help = QAction(
            QIcon(":/images/themes/default/mActionHelpContents.svg"),
            f"{__title__} - Documentation",
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(
                currentPage="mOptionsPage{}".format(__title__)
            )
        )
        self.iface.addToolBar(self.toolbar)
        # -- Menu
        self.iface.addPluginToMenu(__title__, self.action_new_survey_dialog)
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)

        self.dlg_new_survey.button_box.button(QDialogButtonBox.Ok).clicked.connect(
            self.button_box_ok_new_survey_clicked
        )

        self.update_topaze_gui()

    def update_topaze_gui(self):
        self.clean_topaze_gui()
        if self._observations_layer:
            self.action_process_field_codification = QAction(
                QIcon(":/resources/images/update_obs.svg"),
                self.tr("Process field codification", context="TopazePlugin"),
                self.iface.mainWindow(),
            )
            self.action_process_field_codification.triggered.connect(
                self.process_field_codification
            )
            self.action_process_field_codification.setEnabled(True)
            # self.toolbar.addAction(self.action_process_field_codification)

            self.action_edit_observations = QAction(
                QIcon(":plugins/topaze/resources/images/default_icon.png"),
                self.tr("Edit observations", context="TopazePlugin"),
                self.iface.mainWindow(),
            )
            self.action_edit_observations.triggered.connect(self.edit_observations)
            self.action_edit_observations.setEnabled(True)
            # self.toolbar.addAction(self.action_edit_observations)

            self.action_compute_reference_points = QAction(
                QIcon(":plugins/topaze/resources/images/default_icon.png"),
                self.tr("Compute reference points", context="TopazePlugin"),
                self.iface.mainWindow(),
            )
            self.action_compute_reference_points.triggered.connect(
                self.compute_reference_points
            )
            self.action_compute_reference_points.setEnabled(True)
            # self.toolbar.addAction(self.action_compute_reference_points)

            self.action_update_observations_dialog = QAction(
                QIcon(":plugins/topaze/resources/images/default_icon.png"),
                self.tr("Update observations", context="TopazePlugin"),
                self.iface.mainWindow(),
            )
            self.action_update_observations_dialog.triggered.connect(
                self.update_observations_dialog
            )
            self.action_update_observations_dialog.setEnabled(True)
            # self.toolbar.addAction(self.action_update_observations_dialog)

            self.action_import_points_dialog = QAction(
                QIcon(":plugins/topaze/resources/images/default_icon.png"),
                self.tr("Import points", context="TopazePlugin"),
                self.iface.mainWindow(),
            )
            self.action_import_points_dialog.triggered.connect(
                self.import_points_dialog
            )
            self.action_import_points_dialog.setEnabled(True)
            # self.toolbar.addAction(self.action_import_points_dialog)

            self.action_compute_points = QAction(
                QIcon(":plugins/topaze/resources/images/topaze.png"),
                self.tr("Compute points", context="TopazePlugin"),
                self.iface.mainWindow(),
            )
            self.action_compute_points.triggered.connect(self.compute_points)
            self.action_compute_points.setEnabled(True)
            # self.toolbar.addAction(self.action_compute_points)

            self.action_purge_observations = QAction(
                QIcon(":plugins/topaze/resources/images/default_icon.png"),
                self.tr("Purge observations", context="TopazePlugin"),
                self.iface.mainWindow(),
            )
            self.action_purge_observations.triggered.connect(self.purge_observations)
            self.action_purge_observations.setEnabled(True)
            # self.toolbar.addAction(self.action_purge_observations)

            # -- Menu
            self.iface.addPluginToMenu(
                __title__, self.action_update_observations_dialog
            )
            self.iface.addPluginToMenu(__title__, self.action_edit_observations)
            self.iface.addPluginToMenu(__title__, self.action_import_points_dialog)
            self.iface.addPluginToMenu(__title__, self.action_compute_reference_points)
            self.iface.addPluginToMenu(__title__, self.action_compute_points)
            self.iface.addPluginToMenu(__title__, self.action_purge_observations)
            self.dlg_field_data.button_box.button(
                QDialogButtonBox.Save
            ).clicked.connect(self.button_box_save_field_data_clicked)
            self.dlg_import_points.button_box.button(
                QDialogButtonBox.Open
            ).clicked.connect(self.button_box_import_points_clicked)
            self.dlg_edit_obs.pushButton_add.pressed.connect(
                self.on_push_button_add_obs_pressed
            )
            self.dlg_edit_obs.pushButton_change.pressed.connect(
                self.on_push_button_change_obs_pressed
            )
            self.dlg_edit_obs.pushButton_delete.pressed.connect(
                self.on_push_button_delete_obs_pressed
            )
            self.dlg_edit_obs.pushButton_save.pressed.connect(
                self.on_push_button_save_obs_data_pressed
            )
            self.dlg_edit_obs.pushButton_print.pressed.connect(
                self.on_push_button_print_selected_obs_pressed
            )
            self.dlg_edit_obs.pushButton_cancel.pressed.connect(
                self.on_push_button_cancel_edition_pressed
            )
            self.dlg_edit_obs.closeEvent = self.on_edit_observations_close

    def unload(self):
        print("unload")
        """Cleans up when plugin is disabled/uninstalled."""
        self.clean_topaze_gui()
        try:
            self.iface.removePluginMenu(__title__, self.action_new_survey_dialog)
            self.iface.removePluginMenu(__title__, self.action_help)
            self.iface.removePluginMenu(__title__, self.action_settings)
        except Exception as e:
            print(str(e))
        else:
            # -- Clean up toolbar
            self.iface.helpToolBar().parentWidget().removeToolBar(self.toolbar)
            del self.toolbar

            # -- Clean up preferences panel in QGIS settings
            self.iface.unregisterOptionsWidgetFactory(self.options_factory)
            # remove actions
            del self.action_help
            del self.action_settings
            del self.action_new_survey_dialog

    def clean_topaze_gui(self):
        try:
            # -- Clean up menu
            self.iface.removePluginMenu(__title__, self.action_compute_points)
            self.iface.removePluginMenu(__title__, self.action_compute_reference_points)
            self.iface.removePluginMenu(__title__, self.action_edit_observations)
            self.iface.removePluginMenu(
                __title__, self.action_update_observations_dialog
            )
            self.iface.removePluginMenu(__title__, self.action_import_points_dialog)
            self.iface.removePluginMenu(__title__, self.action_purge_observations)
        except Exception as e:
            print(str(e))
        else:
            # remove actions
            del self.action_compute_points
            del self.action_compute_reference_points
            del self.action_edit_observations
            del self.action_update_observations_dialog
            del self.action_import_points_dialog
            del self.action_purge_observations

    def run(self):
        """Main process.

        :raises Exception: if there is no item in the feed
        """
        print("run")
        try:
            self.log(
                message=self.tr(
                    text="Everything ran OK.",
                    context="TopazePlugin",
                ),
                log_level=3,
                push=False,
            )
        except Exception as err:
            self.log(
                message=self.tr(
                    text="Houston, we've got a problem: {}".format(err),
                    context="TopazePlugin",
                ),
                log_level=2,
                push=True,
            )

    def topaze_project_read(self):
        print("topaze_project_read")
        self._obs_edited = False
        self._ptopo_array = []
        layers = QgsProject.instance().mapLayersByName(self.PTOPO_LAYER_NAME)
        if layers:
            self._ptopo_layer = layers[0]
        else:
            self.iface.messageBar().pushMessage(
                self.tr("PTOPO table missing", context="TopazePlugin"), "", Qgis.Info
            )
            self._ptopo_layer = None
        layers = QgsProject.instance().mapLayersByName(self.OBSERVATIONS_LAYER_NAME)
        if layers:
            self._observations_layer = layers[0]
        else:
            self.iface.messageBar().pushMessage(
                self.tr("Observations table missing", context="TopazePlugin"),
                "",
                Qgis.Info,
            )
            self._observations_layer = None
        self.update_topaze_gui()

    def topaze_project_cleared(self):
        pass

    def create_points(self, refresh: bool = True):
        """Create points"""
        print("create_points")
        try:
            nb_created = nb_updated = 0
            features = []
            self._ptopo_layer.startEditing()
            for ptopo in self._ptopo_array:
                feature = None
                if TopazeUtils.ptopo_exists_in_layer(
                    ptopo.matricule, self._ptopo_layer
                ):
                    feature = TopazeUtils.get_feature_by_matricule(
                        ptopo.matricule, ptopo_layer=self._ptopo_layer
                    )
                    old_ptopo = TopazeUtils.get_ptopo_by_matricule(
                        ptopo.matricule, ptopo_layer=self._ptopo_layer
                    )
                    x, y, z = TopazeUtils.process_duplicated(
                        old_ptopo, ptopo, 0.02, 0.007
                    )
                    ptopo.x, ptopo.y, ptopo.z = x, y, z
                if ptopo.z is not None:
                    wkt = f"Point({ptopo.x} {ptopo.y} {ptopo.z})"
                else:
                    wkt = f"Point({ptopo.x} {ptopo.y} {PtopoConst.NO_Z})"
                geom = QgsGeometry.fromWkt(wkt)
                if feature:
                    feature["type_pt"] = ptopo.type
                    feature["prec_xy"] = ptopo.prec_xy
                    feature["prec_z"] = ptopo.prec_z
                    # codes has been added after first publishing
                    if ptopo.codes:
                        i = self._ptopo_layer.fields().indexFromName("codes")
                        if i >= 0:
                            feature["codes"] = ptopo.codes
                    self._ptopo_layer.updateFeature(feature)
                    nb_updated = nb_updated + 1
                else:
                    feature_dict = {}
                    i = self._ptopo_layer.fields().indexFromName("matricule")
                    if i >= 0:
                        feature_dict.update({i: ptopo.matricule})
                    i = self._ptopo_layer.fields().indexFromName("type_pt")
                    if i >= 0:
                        feature_dict.update({i: ptopo.type})
                    i = self._ptopo_layer.fields().indexFromName("prec_xy")
                    if i >= 0:
                        feature_dict.update({i: ptopo.prec_xy})
                    i = self._ptopo_layer.fields().indexFromName("prec_z")
                    if i >= 0:
                        feature_dict.update({i: ptopo.prec_z})
                    i = self._ptopo_layer.fields().indexFromName("codes")
                    if i >= 0:
                        if ptopo.codes:
                            feature_dict.update({i: ptopo.codes})

                    feature = QgsVectorLayerUtils.createFeature(
                        self._ptopo_layer, geom, feature_dict
                    )
                    features.append(feature)
                    nb_created = nb_created + 1

            self._ptopo_layer.commitChanges()
            if len(features) > 0:
                self._ptopo_layer.dataProvider().addFeatures(features)

                if refresh:
                    # If caching is enabled, a simple canvas refresh might not be sufficient
                    # to trigger a redraw and you must clear the cached image for the layer
                    if self.iface.mapCanvas().isCachingEnabled():
                        self._ptopo_layer.triggerRepaint()
                    else:
                        self.iface.mapCanvas().refresh()

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr("Unable to add points", context="TopazePlugin"),
                str(e),
                Qgis.Warning,
            )

        else:
            return nb_created, nb_updated

    def compute_details_from_station(
        self, station, station_index, obs_array, prec_xy, prec_z
    ):
        if station.x is None or station.y is None:
            pt = TopazeUtils.get_ptopo_by_matricule(
                station.matricule, self._ptopo_array, self._ptopo_layer
            )
            try:
                station.x = pt.x
                station.y = pt.y
                station.z = pt.z
            except Exception as e:
                self.iface.messageBar().pushMessage(
                    self.tr("Undetermined station", context="TopazePlugin"),
                    "Import/input coordinates, please",
                    Qgis.Critical,
                )
                return

        references, end_index = TopazeUtils.find_references_in_obs_array(
            obs_array, station_index + 1
        )
        TopazeUtils.get_coordinates_from_references(
            references, self._ptopo_array, self._ptopo_layer
        )
        if station.v0 is None:
            v0 = TopazeCalculator.compute_bearing_pq(station, references)
            station.v0 = v0
        if station.v0 is None:
            station.v0 = 0.0
        next_idx = station_index
        to_compute = True
        for obs in obs_array[station_index + 1 : end_index]:
            next_idx = next_idx + 1
            if obs.type == "st":
                to_compute = False
                break
            elif obs.type in ["pnt", "xyz"]:
                x, y, z = TopazeCalculator.obs_to_xyz(station, obs)
                if x is not None and y is not None:
                    ptopo = Ptopo(
                        obs.matricule, "P", x, y, z, obs.codes, prec_xy, prec_z
                    )
                    try:
                        if obs_array[next_idx].type == "code":
                            ptopo.codes = obs_array[next_idx].codes
                    except Exception:
                        pass
                    if TopazeUtils.ptopo_exists_in_array(
                        ptopo.matricule, self._ptopo_array
                    ):
                        old_ptopo = TopazeUtils.get_ptopo_by_matricule(
                            ptopo.matricule, ptopo_array=self._ptopo_array
                        )
                        x, y, z = TopazeUtils.process_duplicated(
                            old_ptopo, ptopo, 0.02, 0.007
                        )
                        status = TopazeUtils.update_xyz_in_array(
                            self._ptopo_array, ptopo.matricule, x, y, z
                        )
                        if status:
                            print(f"Ptopo {ptopo.matricule} updated in array")
                        else:
                            ptopo.x, ptopo.y, ptopo.z = x, y, z
                            self._ptopo_array.append(ptopo)
                    else:
                        self._ptopo_array.append(ptopo)
                        print(f"Ptopo {ptopo.matricule} added in array")

    def compute_points(self, type: str = "P", prec_xy: str = "A", prec_z: str = "A"):
        """Compute surveyed points"""
        print("compute_points")

        try:
            field_data = ""
            self._ptopo_array.clear
            if self._observations_layer:
                feature_dict = {}
                idx_obs_column = self._observations_layer.fields().indexFromName("obs")
                obs_iter = self._observations_layer.getFeatures()
                if obs_iter.isValid():
                    feature = QgsFeature()
                    if obs_iter.nextFeature(feature):
                        if idx_obs_column >= 0:
                            field_data = feature.attribute("obs")

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr("Unable to get field data", context="TopazePlugin"),
                str(e),
                Qgis.Info,
            )

        else:
            field_rows = field_data.splitlines()
            station = None
            obs_array = []
            for row in field_rows:
                type_obs, type_pt = TopazeUtils.types_obs(row)
                obs_dict = TopazeUtils.obs_str_to_dict(row)
                obs = Observation(type_obs, obs_dict)
                obs_array.append(obs)

            nb_stations = 0
            station_index = -1
            while True:
                station, station_index = TopazeUtils.find_station_in_obs_array(
                    obs_array, station_index + 1
                )
                if station is None:
                    if nb_stations < 1:
                        self.iface.messageBar().pushMessage(
                            self.tr("No station found ", context="TopazePlugin"),
                            self.tr(
                                "Check/modify observations", context="TopazePlugin"
                            ),
                            Qgis.Critical,
                        )
                        return
                    break
                nb_stations += 1
                self.compute_details_from_station(
                    station, station_index, obs_array, prec_xy, prec_z
                )

            (n_cre, n_upd) = self.create_points()
            self.iface.messageBar().pushMessage(
                self.tr(
                    "{} surveyed point(s) computed".format(len(self._ptopo_array)),
                    context="TopazePlugin",
                ),
                self.tr(
                    " ({} created, {} updated)".format(n_cre, n_upd),
                    context="TopazePlugin",
                ),
                Qgis.Info,
            )

        ...

    def compute_reference_points(self):
        """Compute topographic canvas point"""
        print("TODO : compute_reference_points")
        ...

    def new_survey_dialog(self):
        """Open dialog New survey"""
        print("new_survey_dialog")
        self.dlg_new_survey.show()

    def new_survey(self, to_path: str):
        """Create new survey by copying template directory"""
        print("new_survey")
        try:
            if os.path.exists(to_path):
                shutil.rmtree(to_path)
            from_path = DIR_PLUGIN_ROOT / "resources/templates/surveying"
            shutil.copytree(from_path, to_path)
            old_qgs = os.path.join(to_path, "surveying.qgs")
            new_qgs = os.path.join(
                to_path, os.path.basename(os.path.normpath(to_path)) + ".qgs"
            )
            os.rename(old_qgs, new_qgs)

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr("Unable to create new survey ", context="TopazePlugin"),
                self.tr(" in {}".format(to_path), context="TopazePlugin"),
                Qgis.Critical,
            )
        else:
            self.iface.messageBar().pushMessage(
                self.tr("New survey created ", context="TopazePlugin"),
                self.tr(" in {}".format(to_path), context="TopazePlugin"),
                Qgis.Success,
            )

    def update_observations_dialog(self):
        """Open dialog Update observations"""
        print("update_observations_dialog")
        self.dlg_field_data.show()

    def update_observations(self, fullfilepath: str):
        """Update 'observations' table from text file"""
        print("update_observations")
        # open .obs file read fied data
        try:
            field_data_type = TopazeUtils.get_field_data_type(fullfilepath)
            if field_data_type not in ["TRIMBLE", "LEICA"]:
                self.iface.messageBar().pushMessage(
                    self.tr(
                        "Unable to detect format from {}".format(fullfilepath),
                        context="TopazePlugin",
                    ),
                    "",
                    Qgis.Critical,
                )
                return

            try:
                inputclass = BUILTIN_INPUT_FORMATS[field_data_type]
            except KeyError as e:
                self.iface.messageBar().pushMessage(
                    self.tr(
                        "{} is not a valid input format".format(field_data_type),
                        context="TopazePlugin",
                    ),
                    "",
                    Qgis.Critical,
                )
                return
            else:
                if isinstance(inputclass, tuple):
                    try:
                        # builtin format parser
                        mod, cls, name = inputclass
                        format_parser = getattr(
                            importlib.import_module(
                                f"topaze.totalopenstation.formats.{mod}"
                            ),
                            cls,
                        )
                    except ImportError as e:
                        self.iface.messageBar().pushMessage(
                            self.tr(
                                "Error importing {} from {}".format(cls, mod),
                                context="TopazePlugin",
                            ),
                            str(e),
                            Qgis.Critical,
                        )
                        return

            with open(
                fullfilepath, "r", encoding=TopazeUtils.find_file_encoding(fullfilepath)
            ) as field_file:
                fp = format_parser(field_file.read(), point_starts_on="5", use_all=True)
                field_file.close()
                rl = fp.raw_line

                of = OutputFormat(rl)
                new_survey = of.process()

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr(
                    "Unable to open {}".format(fullfilepath), context="TopazePlugin"
                ),
                str(e),
                Qgis.Warning,
            )

        else:
            try:
                db_field_data = ""
                if self._observations_layer:
                    feature_dict = {}
                    db_field_data = self.get_observations_from_db(False)
                    if len(db_field_data):
                        new_field_data = db_field_data + "\n" + new_survey
                    else:
                        new_field_data = new_survey
                    self.set_observations_in_db(new_field_data, db_field_data)
                    idx_obs_column = self._observations_layer.fields().indexFromName(
                        "obs"
                    )
                    obs_iter = self._observations_layer.getFeatures()
                    if obs_iter.isValid():
                        feature = QgsFeature()
                        if obs_iter.nextFeature(feature):
                            if idx_obs_column >= 0:
                                self._observations_layer.startEditing()
                                self._observations_layer.changeAttributeValue(
                                    feature.id(),
                                    idx_obs_column,
                                    new_field_data,
                                    db_field_data,
                                )
                                self._observations_layer.commitChanges()

            except Exception as e:
                self.iface.messageBar().pushMessage(
                    self.tr(
                        "Unable to update observations with field data",
                        context="TopazePlugin",
                    ),
                    str(e),
                    Qgis.Warning,
                )
            else:
                self.iface.messageBar().pushMessage(
                    self.tr("Observations updated", context="TopazePlugin"),
                    self.tr(
                        " with field data from {}".format(fullfilepath),
                        context="TopazePlugin",
                    ),
                    Qgis.Success,
                )

    def import_points_dialog(self):
        """Open dialog Import points"""
        self.dlg_import_points.show()

    def import_points(self, fullfilepath: str):
        """Import points  from text file"""
        try:
            if fullfilepath.lower().endswith(".jsi") or fullfilepath.lower().endswith(
                ".txt"
            ):
                from topaze.totalopenstation.formats.topstation_jsi import FormatParser
            with open(
                fullfilepath, "r", encoding=TopazeUtils.find_file_encoding(fullfilepath)
            ) as pointsfile:
                fp = FormatParser(pointsfile.read(), ptopo_only=True)
                pointsfile.close()

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr(
                    "Unable to open {}".format(fullfilepath), context="TopazePlugin"
                ),
                str(e),
                Qgis.Info,
            )

        else:
            # get points data
            try:
                rl = fp.raw_line
                from topaze.totalopenstation.output.tops_ptopo import OutputFormat

                of = OutputFormat(rl)
                self._ptopo_array.clear
                self._ptopo_array = of.process()
                nb = len(self._ptopo_array)
                n_cre, n_upd = self.create_points()
            except Exception as e:
                self.iface.messageBar().pushMessage(
                    self.tr(
                        "Unable to import points from {}".format(fullfilepath),
                        context="TopazePlugin",
                    ),
                    str(e),
                    Qgis.Warning,
                )
            else:
                self.iface.messageBar().pushMessage(
                    self.tr(
                        "{} point(s) imported from {}".format(nb, fullfilepath),
                        context="TopazePlugin",
                    ),
                    self.tr(
                        " ({} created, {} updated)".format(n_cre, n_upd),
                        context="TopazePlugin",
                    ),
                    Qgis.Success,
                )

    def get_observations_from_db(self, returns_rows: bool = True):
        try:
            field_data = ""
            if self._observations_layer:
                feature_dict = {}
                idx_obs_column = self._observations_layer.fields().indexFromName("obs")
                obs_iter = self._observations_layer.getFeatures()
                if obs_iter.isValid():
                    feature = QgsFeature()
                    if obs_iter.nextFeature(feature):
                        if idx_obs_column >= 0:
                            field_data = feature.attribute("obs")

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr("Unable to get field data", context="TopazePlugin"),
                str(e),
                Qgis.Info,
            )

        else:
            if returns_rows:
                field_rows = field_data.splitlines()
                return field_rows
            else:
                return field_data
        return None

    def set_observations_in_db(self, new_data, old_data):
        try:
            feature_dict = {}
            idx_obs_column = self._observations_layer.fields().indexFromName("obs")
            obs_iter = self._observations_layer.getFeatures()
            if obs_iter.isValid():
                feature = QgsFeature()
                if obs_iter.nextFeature(feature):
                    if idx_obs_column >= 0:
                        self._observations_layer.startEditing()
                        self._observations_layer.changeAttributeValue(
                            feature.id(), idx_obs_column, new_data, old_data
                        )
                        self._observations_layer.commitChanges()
        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr(
                    "Unable to set field observations in table", context="TopazePlugin"
                ),
                "",
                Qgis.Critical,
            )

    def edit_observations(self):
        """Load observations edition dialog"""
        print("edit_observations")

        self.dlg_edit_obs.comboBox_type_point.setEnabled(False)
        self.dlg_edit_obs.lineEdit_matricule.setEnabled(False)
        self.dlg_edit_obs.lineEdit_horizontal_angle.setEnabled(False)
        self.dlg_edit_obs.lineEdit_vertical_angle.setEnabled(False)
        self.dlg_edit_obs.lineEdit_sloped_distance.setEnabled(False)
        self.dlg_edit_obs.lineEdit_x.setEnabled(False)
        self.dlg_edit_obs.lineEdit_y.setEnabled(False)
        self.dlg_edit_obs.lineEdit_z.setEnabled(False)
        self.dlg_edit_obs.lineEdit_target_height.setEnabled(False)
        self.dlg_edit_obs.lineEdit_code.setEnabled(False)
        self.dlg_edit_obs.lineEdit_comment.setEnabled(False)

        self.dlg_edit_obs.comboBox_type_point.setVisible(False)
        self.dlg_edit_obs.lineEdit_matricule.setVisible(False)
        self.dlg_edit_obs.lineEdit_horizontal_angle.setVisible(False)
        self.dlg_edit_obs.lineEdit_vertical_angle.setVisible(False)
        self.dlg_edit_obs.lineEdit_sloped_distance.setVisible(False)
        self.dlg_edit_obs.lineEdit_x.setVisible(False)
        self.dlg_edit_obs.lineEdit_y.setVisible(False)
        self.dlg_edit_obs.lineEdit_z.setVisible(False)
        self.dlg_edit_obs.lineEdit_target_height.setVisible(False)
        self.dlg_edit_obs.lineEdit_code.setVisible(False)
        self.dlg_edit_obs.lineEdit_comment.setVisible(False)

        self.dlg_edit_obs.label_horizontal_angle.setVisible(False)
        self.dlg_edit_obs.label_vertical_angle.setVisible(False)
        self.dlg_edit_obs.label_sloped_distance.setVisible(False)
        self.dlg_edit_obs.label_x.setVisible(False)
        self.dlg_edit_obs.label_y.setVisible(False)
        self.dlg_edit_obs.label_z.setVisible(False)
        self.dlg_edit_obs.label_target_height.setVisible(False)
        self.dlg_edit_obs.label_code.setVisible(False)
        self.dlg_edit_obs.label_comment.setVisible(False)

        field_rows = self.get_observations_from_db()
        self.dlg_edit_obs.load_observations(field_rows)

        self._obs_edited = False
        self.dlg_edit_obs.pushButton_save.setEnabled(False)
        self.dlg_edit_obs.show()

        # self.dlg_edit_obs.comboBox_type_obs.setCurrentIndex(0)

        self.dlg_edit_obs.listWidget_observations.currentItemChanged.connect(
            self.on_list_obs_current_item_changed
        )

        self.dlg_edit_obs.comboBox_type_obs.currentTextChanged.connect(
            self.on_combo_type_obs_current_text_changed
        )

    def process_field_codification(self):
        print("TODO : process_field_codification")
        ...

    def purge_observations(self):
        """Remove all observations"""
        print("purge_observations")
        try:
            db_field_data = ""
            if self._observations_layer:
                idx_obs_column = self._observations_layer.fields().indexFromName("obs")
                obs_iter = self._observations_layer.getFeatures()
                if obs_iter.isValid():
                    feature = QgsFeature()
                    if obs_iter.nextFeature(feature):
                        if idx_obs_column >= 0:
                            db_field_data = feature.attribute("obs")
                            new_field_data = ""
                            self._observations_layer.startEditing()
                            self._observations_layer.changeAttributeValue(
                                feature.id(),
                                idx_obs_column,
                                new_field_data,
                                db_field_data,
                            )
                            self._observations_layer.commitChanges()

        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr("Unable to erase observations", context="TopazePlugin"),
                str(e),
                Qgis.Warning,
            )

        else:
            self._obs_edited = False
            self.dlg_edit_obs.pushButton_save.setEnabled(False)
            self.iface.messageBar().pushMessage(
                self.tr("Observations erased", context="TopazePlugin"), "", Qgis.Info
            )

    def button_box_ok_new_survey_clicked(self):
        self.new_survey(self.dlg_new_survey.lineEdit.text())

    def button_box_save_field_data_clicked(self):
        if len(self.dlg_field_data.lineEdit.text()) > 0:
            self.update_observations(self.dlg_field_data.lineEdit.text())

    def button_box_import_points_clicked(self):
        if len(
            self.dlg_import_points.lineEdit.text()
        ) > 0 and self.dlg_import_points.lineEdit.text()[-4:].lower() in [
            ".csv",
            ".jsi",
            ".txt",
        ]:
            self.import_points(self.dlg_import_points.lineEdit.text())

    def on_push_button_add_obs_pressed(self):
        """add new observation"""
        dico = self.dlg_edit_obs.observation_fields_to_dict(
            self.dlg_edit_obs.comboBox_type_obs.currentText(),
            self.dlg_edit_obs.comboBox_type_point.currentText(),
        )
        rows = TopazeUtils.dict_to_obs_str(dico)
        if len(rows) == 1:
            try:
                idx = self.dlg_edit_obs.listWidget_observations.currentIndex().row()
                if idx == self.dlg_edit_obs.listWidget_observations.count() - 1:
                    self.dlg_edit_obs.listWidget_observations.addItem(rows[0])
                else:
                    self.dlg_edit_obs.listWidget_observations.insertItem(
                        idx + 1, rows[0]
                    )
            except Exception as e:
                self.iface.messageBar().pushMessage(
                    self.tr("Unable to add observation", context="TopazePlugin"),
                    str(e),
                    Qgis.Warning,
                )
            else:
                self.dlg_edit_obs.listWidget_observations.setCurrentRow(idx + 1)
        elif len(rows) > 1:
            ...
        else:
            ...
        ...
        self._obs_edited = True
        self.dlg_edit_obs.pushButton_save.setEnabled(True)

    def on_push_button_change_obs_pressed(self):
        """change observation"""
        dico = self.dlg_edit_obs.observation_fields_to_dict(
            self.dlg_edit_obs.comboBox_type_obs.currentText(),
            self.dlg_edit_obs.comboBox_type_point.currentText(),
        )
        rows = TopazeUtils.dict_to_obs_str(dico)
        if len(rows) == 1:
            if self._current_obs is not None:
                self._current_obs.setText(rows[0])
        elif len(rows) > 1:
            ...
        else:
            ...
        self._obs_edited = True
        self.dlg_edit_obs.pushButton_save.setEnabled(True)

    def on_push_button_delete_obs_pressed(self):
        """delete observation"""
        current_row = self.dlg_edit_obs.listWidget_observations.currentRow()
        item = self.dlg_edit_obs.listWidget_observations.takeItem(current_row)
        self._obs_edited = True
        self.dlg_edit_obs.pushButton_save.setEnabled(True)

    def on_push_button_save_obs_data_pressed(self):
        """save observations data"""
        try:
            rows = [
                self.dlg_edit_obs.listWidget_observations.item(x).text()
                for x in range(self.dlg_edit_obs.listWidget_observations.count())
            ]
            new_data = "\n".join(rows)
            self.set_observations_in_db(new_data, None)
        except Exception as e:
            self.iface.messageBar().pushMessage(
                self.tr("Unable to save field observations", context="TopazePlugin"),
                str(e),
                Qgis.Warning,
            )
        else:
            self.iface.messageBar().pushMessage(
                self.tr("Field observations data saved", context="TopazePlugin"),
                "",
                Qgis.Success,
            )
            self._obs_edited = False
            self.dlg_edit_obs.pushButton_save.setEnabled(False)

    def on_push_button_print_selected_obs_pressed(self):
        """TODO : print selected observations"""
        ...

    def on_edit_observations_close(self, result):
        self.on_push_button_cancel_edition_pressed()

    def on_push_button_cancel_edition_pressed(self):
        """cancel changes and close dialog"""
        if self._obs_edited:
            if self.show_alert(
                self.tr("Edit observations"),
                self.tr("Do you want to save or cancel and drop your changes ?"),
            ):
                self.on_push_button_save_obs_data_pressed()
        self._obs_edited = False
        self.dlg_edit_obs.pushButton_save.setEnabled(False)
        self.dlg_edit_obs.hide()

    def on_list_obs_current_item_changed(self, current, previous):
        """process current row changed"""
        self._current_obs = current
        self._previous_obs = previous
        if current is not None:
            type_obs, type_pt = TopazeUtils.types_obs(current.text())
            if type_pt is not None:
                print(f"type_obs {type_obs}, type_pt {type_pt}")
            else:
                print(f"type_obs {type_obs}")
            self.dlg_edit_obs.update_combobox_types(type_obs, type_pt)
            self.dlg_edit_obs.load_observation_fields(
                type_obs, TopazeUtils.obs_str_to_dict(current.text())
            )

    def on_combo_type_obs_current_text_changed(self, type_obs):
        """process curent type obs changed"""
        self.dlg_edit_obs.update_state_of_input_fields(type_obs)

    def show_alert(self, title, msg):
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setText(msg)
        msgBox.setWindowTitle(title)
        msgBox.setStandardButtons(QMessageBox.Save | QMessageBox.Cancel)
        # msgBox.buttonClicked.connect(msgButtonClick)
        returnValue = msgBox.exec()
        if returnValue == QMessageBox.Save:
            return True
        else:
            return False
